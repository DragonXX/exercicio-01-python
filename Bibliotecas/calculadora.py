from funcoes_algebrica import soma
from funcoes_saude import calcular_imc
print ('Qual função quer realizar: soma ou imc?')
resposta = input("Digite 'soma' ou 'imc'\n")
valor1 = float(input('Digite o valor 1: '))
valor2 = float(input('Digite o valor 2: '))
if resposta == 'soma':
    print(soma(valor1, valor2))
elif resposta == 'imc':
    print(calcular_imc(valor1, valor2))
else:
    print('Resposta inválida!')