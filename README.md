# Estudar python
## Criar alguns artefatos.
1. Criar um conjunto de modulos e bibliotecas.
- Módulo calcular IMC.
- Módulo Matemático (algebra básica)
- Modulo Científico (raiz quadrada, função exponêncial)
- Criar uma tela com Menu de acesso.

## Revisando os conteúdos:

0. Práticas de DevOps 
  1. Ambientes virtuais para trabalhar com python
  2. GitOps
1. Criar módulos e funções
2. Padrões de projeto e desenvolvimento
3. Planejamento para execução dos Jobs

## Conteúdos para aprendizado:

0. Programação em par (Seguir ideia do fluxo TDD - Test Driver Develompent)
